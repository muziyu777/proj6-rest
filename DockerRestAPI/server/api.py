import flask
from flask import Flask, redirect, url_for, request, render_template
from flask_restful import Resource, Api
import arrow  
import acp_times  
import config
import logging
import pymongo
from pymongo import MongoClient
import os

app = Flask(__name__)
api = Api(app)


CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
# client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
client = MongoClient('mongodb://mongodb:27017/')
db = client.tododb
db.tododb.delete_many({}) 
DEFAULT = 20

ITEMS = {}

@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    
    return flask.render_template('404.html'), 404



@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    dist = request.args.get('dist', type=float)
    begindate = request.args.get('bd', "", type=str)
    begintime = request.args.get('bt', "", type=str)
    timer = arrow.get(begindate + " " + begintime, 'YYYY-MM-DD HH:mm').isoformat()

    open_time = acp_times.open_time(km, dist, timer)
    close_time = acp_times.close_time(km, dist, timer)
    rslt = { "open": open_time, "close": close_time}
    row = request.args.get("row")
    ITEMS[row] = [km, open_time, close_time]  

    return flask.jsonify(result=rslt)

@app.route("/submit")
def _submit():
    
    try:
        if len(ITEMS) == 0:
            rslt = {"suc": False}
            return flask.jsonify(result=rslt)
            
        db.tododb.delete_many({})    
        for i in ITEMS.values():
            item_doc = {
                'km': i[0],
                'open_time': i[1],
                'close_time': i[2]
            }
            db.tododb.insert_one(item_doc)

        rslt = {"suc": True}
    except:
        rslt = {"suc": False}

    return flask.jsonify(result=rslt)

@app.route("/display")
def _display():
    ITEMS.clear()
    _items = db.tododb.find()
    items = [item for item in _items]

    for item in items:
        item['open_time'] = arrow.get(item['open_time']).format('dddd D/M HH:mm')
        item['close_time'] = arrow.get(item['close_time']).format('dddd D/M HH:mm')

    return render_template('display.html', items=items)

#try to use same way for the example
#source:https://stackoverflow.com/questions/8109122/how-to-sort-mongodb-with-pymongo
class ListAll(Resource):
    def get(self):
        top = request.args.get("top")

        if(top==None):
            top = DEFAULT

        _items = db.tododb.find().sort("open_time", pymongo.ASCENDING).limit(int(top))
        items = [item for item in _items]

        return {
            'open_time': [arrow.get(item['open_time']).format('dddd D/M HH:mm') for item in items],
            'close_time': [arrow.get(item['close_time']).format('dddd D/M HH:mm') for item in items]
        }
api.add_resource(ListAll, '/listAll')

class ListAllJSON(Resource):
    def get(self):
        top = request.args.get("top")

        if(top==None):
            top = DEFAULT

        _items = db.tododb.find().sort("open_time", pymongo.ASCENDING).limit(int(top))
        items = [item for item in _items]

        return {
            'open_time': [arrow.get(item['open_time']).format('dddd D/M HH:mm') for item in items],
            'close_time': [arrow.get(item['close_time']).format('dddd D/M HH:mm') for item in items]
        }

api.add_resource(ListAllJSON, '/listAll/json')

class ListAllCSV(Resource):
    def get(self):
        top = request.args.get("top")
        if(top==None):
            top = DEFAULT

        _items = db.tododb.find().sort("open_time", pymongo.ASCENDING).limit(int(top))
        items = [item for item in _items]

        opencsv =""
        closecsv=""
        for item in items:
            opencsv += arrow.get(item['open_time']).format('dddd D/M HH:mm') + ','
            closecsv+= arrow.get(item['close_time']).format('dddd D/M HH:mm') + ','
        return (opencsv+closecsv)
api.add_resource(ListAllCSV, '/listAll/csv')

class OpenOnly(Resource):
    def get(self):
        top = request.args.get("top")
        if(top==None):
            top = DEFAULT

        _items = db.tododb.find().sort("open_time", pymongo.ASCENDING).limit(int(top))
        items = [item for item in _items]

        return {
            'open_time': [arrow.get(item['open_time']).format('dddd D/M HH:mm') for item in items]
        }
    
api.add_resource(OpenOnly, '/listOpenOnly')
class OpenJSONOnly(Resource):
    def get(self):
        top = request.args.get("top")

        if(top==None):
            top = DEFAULT

        _items = db.tododb.find().sort("open_time", pymongo.ASCENDING).limit(int(top))
        items = [item for item in _items]

        return {
            'open_time': [arrow.get(item['open_time']).format('dddd D/M HH:mm') for item in items]
        }
api.add_resource(OpenJSONOnly, '/listOpenOnly/json')

class OpenCSVOnly(Resource):
    def get(self):
        top = request.args.get("top")

        if(top==None):
            top = DEFAULT

        _items = db.tododb.find().sort("open_time", pymongo.ASCENDING).limit(int(top))
        items = [item for item in _items]

        opencsv = ""
        for item in items:
            opencsv += arrow.get(item['open_time']).format('dddd D/M HH:mm') + ','
        return opencsv
api.add_resource(OpenCSVOnly, '/listOpenOnly/csv')
class CloseOnly(Resource):
    def get(self):
        top = request.args.get("top")

        if(top==None):
            top = DEFAULT

        _items = db.tododb.find().sort("close_time", pymongo.ASCENDING).limit(int(top))
        items = [item for item in _items]

        return {
            'close_time': [arrow.get(item['close_time']).format('dddd D/M HH:mm') for item in items]
        }
api.add_resource(CloseOnly, '/listCloseOnly')
class CloseJSONOnly(Resource):
    def get(self):
        top = request.args.get("top")

        if(top==None):
            top = DEFAULT

        _items = db.tododb.find().sort("close_time", pymongo.ASCENDING).limit(int(top))
        items = [item for item in _items]

        return {
            'close_time': [arrow.get(item['close_time']).format('dddd D/M HH:mm') for item in items]
        }
api.add_resource(CloseJSONOnly, '/listCloseOnly/json')
class CloseCSVOnly(Resource):
    def get(self):
        top = request.args.get("top")

        if(top==None):
            top = DEFAULT

        _items = db.tododb.find().sort("close_time", pymongo.ASCENDING).limit(int(top))
        items = [item for item in _items]

        closecsv = ""
        for item in items:
            closecsv += arrow.get(item['close_time']).format('dddd D/M HH:mm') + ','
        return closecsv
api.add_resource(CloseCSVOnly, '/listCloseOnly/csv')

#############



app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    app.run(port=CONFIG.PORT, host="0.0.0.0")
